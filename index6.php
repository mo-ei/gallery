<?php /* Hier code */
	if ($_GET['dir'] == "")
	{
		$mediaDir =".";
	}
	else
	{
		$mediaDir ="./".$_GET['dir'];
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"> 
		<meta name="theme-color" content="#ababab">
		<link rel="manifest" href="manifest.json">
		<link href="style.css" rel="stylesheet">
		<script type="text/javascript">
			var files = JSON.parse(<?php echo "'".json_encode(scandir($mediaDir, SCANDIR_SORT_ASCENDING))."'"; ?>);
			var mediaDir = <?php echo "'".$mediaDir."/'"; ?>;
			var isKiosk = <?php echo "\"".$_GET['kiosk']."\""; ?> == "true";
		</script>

	    <script src="slide-navigation.js" type="text/javascript"></script>
	</head>
	
	<body>
		<div id="menu" class="control">
			<div class="element" onclick="navigateSlide(-1)">zurück</div>		
			<div class="element" onclick="navigateSlide(0)">nochmal</div>
			<div class="element" onclick="gotoSlide(0)">zum Anfang</div>
			<div class="element" onclick="window.location('index6.php')">zur Übersicht</div>
			<div class="element" onclick="navigateSlide(1)">weiter</div>
		</div>
		
		<div id="primaryMedia-container">
			<video autoplay controls onended="onMediaEnded()" class="media primaryMedia">
				<source>
			</video>
			<img class="media primaryMedia">
			<div id="fileSelectionContainer">
				<?php
					$verz = ".";
					echo '<h1>Auswahl</h1><br>';
					echo "<ol>";
					if (is_dir($verz))
					{
						if ($handle=opendir($verz))
						{
							while(($fle=readdir($handle)) != false)
							{

								if ((filetype($fle) == "dir") and ($fle!='lost+found') and ($fle!='..') and ($fle!='.'))
								{
									echo '<li><a href="index6.php?dir='.$fle.'">'.$fle.'</a>';
								}
							}
							closedir($handle);
						}
					}
					echo "</ol>";						
				?>
			</div>
		</div>
		
		<div id="info" class="control">
			<div id="slide-number-display" class="element">
				Folie&nbsp;<div id="current-slide"></div>&nbsp;von&nbsp;<div id="num-slides"></div>
			</div>
			<div id="player-container" class="element">
				<audio controls onended="onMediaEnded()" class="media secondaryMedia">
					<source type="audio/mpeg">
				</audio>
			</div>
			<form id="settings" class="element">
				<div class="checkbox-wrapper">
					<input type="checkbox" id="settings-autoplay" name="Autoplay">
					<label for="settings-autoplay">Autoplay</label>
				</div>
				<div class="checkbox-wrapper">
					<input type="checkbox" id="settings-autoAdvance" name="Nach Ende der Wiedergabe die nächste Folie anzeigen">
					<label for="settings-autoAdvance">Nach Ende der Wiedergabe die nächste Folie anzeigen</label>
				</div>
			</div>
		</div>
	</body>
</html>