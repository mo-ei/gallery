var img = document.querySelector("img.media");
var video = document.querySelector("video.media");
var audio = document.querySelector("audio.media");
var selectionContainer = document.getElementById("fileSelectionContainer");
var menu = document.getElementById('menu');
var info = document.getElementById("info");
var settings_autoplay = document.getElementById("settings-autoplay");
var settings_autoAdvance = document.getElementById("settings-autoAdvance");
let isKiosk;
let slideIndex = 0;
let mediaDir = "";

let media = [];

console.log("media:");
console.log(media);

function initMedia(files) {
	files.forEach(file => {
		if((/.*?\.(gif|jpe?g|tiff|png|webp|bmp)$/i).test(file)) {
			media.push({imgSrc: file});
		} else if((/.*?\.(mov|avi|wmv|flv|3gp|mp4|mpg)$/i).test(file)) {
			media.push({videoSrc: file});
		} else if((/.*?\.(mp3|wma|aac|wav|flac)$/i).test(file)) {
			media[media.length -1].audioSrc = file;
		}
	})
}

initMedia([
	"https://mo-ei.de/intern/fotos/2016-England/2016-03-24_06-32-37_001.jpg",
	"https://mo-ei.de/intern/fotos/2016-England/2016-03-25_09-03-34_001.jpg",
	"https://mo-ei.de/intern/fotos/2016-England/2016-03-25_10-03-26_001.jpg",
	"https://mo-ei.de/intern/fotos/2016-England/1punkt5stille.mp3",
]);

// Updating static content
updateKiosk();
displayMedia();
document.getElementById("num-slides").innerHTML = media.length;

// Called if any media-playback has finished
function onMediaEnded() {
	var doAutoAdvance = settings_autoAdvance.checked;
	if(doAutoAdvance) setTimeout(navigateSlide(1), 1000);
}

// Updates the page to hide menu and info in kiosk mode
function updateKiosk() {
	if(isKiosk) {
		menu.style.display = "none";
		info.style.display = "none";
		settings_autoplay.checked = true;
		settings_autoAdvance.checked = true;
		video.controls = false;
		
		video.autoplay = true;
		audio.autoplay = true;
	} else {
		menu.style.display = "flex";
		info.style.display = "flex";
		video.controls = true;
	}
	console.log(settings_autoplay.checked);
}

// Navigates a specified number of slides in any direction
function navigateSlide(offset) {
	offset %= media.length;
	slideIndex += offset;
	slideIndex += media.length; // In case slideIndex would have went negative;
	slideIndex %= media.length;
	displayMedia();
}

// Opens a specified slide
function gotoSlide(newIndex) {
	slideIndex = Math.abs(newIndex) % media.length;
	displayMedia();
}

// Displays and updates the media
function displayMedia() {
	// Logging
	console.log("slide index: " + slideIndex);
	
	// Pauses media in order to later change the source
	video.pause();
	audio.pause();
	
	// Displays and updates the primary media source
	if(media[slideIndex].imgSrc) {
		img.style.display = "flex";
		img.src = mediaDir + media[slideIndex].imgSrc;
	} else {
		img.style.display = "none";
		img.src = "";
	}

	if(media[slideIndex].videoSrc) {
		// Hides and unhides appropriate elements and sets the source, loads video, hides audio
		video.style.display = "flex";
		video.setAttribute('src', mediaDir + media[slideIndex].videoSrc);
		//video.load();
	} else {
		video.style.display = "none";
		video.setAttribute('src', "");
	}

	if(media[slideIndex].audioSrc) {
		audio.style.display = "flex";
		audio.setAttribute('src',mediaDir + media[slideIndex].audioSrc);
		audio.load();
		console.log(mediaDir + audioSrc[slideIndex]);
	} else {
		audio.style.display = "none";
	}

	updateAutoplay();
	
	// Updates current slide number
	document.getElementById("current-slide").innerHTML = slideIndex+1;
}

function updateAutoplay() {
	var doAutoplay = settings_autoplay.checked;
	
	if(isKiosk) {
		video.autoplay = doAutoplay;
		audio.autoplay = doAutoplay;
		
		video.load();
		audio.load();
	} else {
		// Plays the videos if autoplay is enabled and the specified media source should be active
		if(doAutoplay && media[slideIndex].videoSrc) video.play(); else video.pause();
		if(doAutoplay && media[slideIndex].audioSrc) audio.play();
	}
}